﻿
namespace LabelDesigner.Parameters
{
    partial class Bitmapped
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bitmapped));
            this.cwidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cheight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.data = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.font = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.orientation = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cwidth
            // 
            this.cwidth.Location = new System.Drawing.Point(137, 162);
            this.cwidth.Name = "cwidth";
            this.cwidth.Size = new System.Drawing.Size(141, 20);
            this.cwidth.TabIndex = 20;
            this.cwidth.Text = "20";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 26);
            this.label1.TabIndex = 19;
            this.label1.Text = "Individual character\r\nwidth (in dots) ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(199, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 35);
            this.button1.TabIndex = 18;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cheight
            // 
            this.cheight.Location = new System.Drawing.Point(137, 126);
            this.cheight.Name = "cheight";
            this.cheight.Size = new System.Drawing.Size(141, 20);
            this.cheight.TabIndex = 17;
            this.cheight.Text = "20";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 26);
            this.label7.TabIndex = 16;
            this.label7.Text = "Individual character\r\nheight (in dots) ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Font name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Values:  10 to 32000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 26);
            this.label3.TabIndex = 23;
            this.label3.Text = "Values: A through Z \r\nand 0 to 9";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Values:  10 to 32000";
            // 
            // data
            // 
            this.data.Location = new System.Drawing.Point(137, 204);
            this.data.Name = "data";
            this.data.Size = new System.Drawing.Size(258, 20);
            this.data.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Data";
            // 
            // font
            // 
            this.font.Location = new System.Drawing.Point(137, 17);
            this.font.Name = "font";
            this.font.Size = new System.Drawing.Size(141, 20);
            this.font.TabIndex = 28;
            this.font.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Orientation";
            // 
            // orientation
            // 
            this.orientation.FormattingEnabled = true;
            this.orientation.Items.AddRange(new object[] {
            "N",
            "R",
            "I",
            "B"});
            this.orientation.Location = new System.Drawing.Point(137, 51);
            this.orientation.Name = "orientation";
            this.orientation.Size = new System.Drawing.Size(141, 21);
            this.orientation.TabIndex = 30;
            this.orientation.Text = "N";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(284, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 65);
            this.label9.TabIndex = 31;
            this.label9.Text = "Values:\r\nN = normal\r\nR = rotated 90 degrees (clockwise)\r\nI = inverted 180 degrees" +
    "\r\nB = read from bottom up, 270 degrees";
            // 
            // Bitmapped
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 302);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.orientation);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.font);
            this.Controls.Add(this.data);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cwidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cheight);
            this.Controls.Add(this.label7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Bitmapped";
            this.Text = "Alphanumeric Data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox cwidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox cheight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox data;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox font;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox orientation;
        private System.Windows.Forms.Label label9;
    }
}